locals;
dynare_config();

% Compile the original model.
dynare('rbc.mod', 'console','noclearall');
truemodel.oo = oo_;
truemodel.M = M_;
truemodel.options = options_;
warning off all,
delete('rbc.m', 'rbc.log', 'rbc_results.mat');
warning on all
clear('oo_', 'M_', 'options_')

% Define indices used for the approximation.
peaopt.idExpectedTerms = strmatch('ExpectedTerm', truemodel.M.endo_names);
peaopt.idExpectedEquations = 10;
peaopt.idEfficiency = strmatch('Efficiency', truemodel.M.endo_names);
peaopt.idCapital = strmatch('Capital', truemodel.M.endo_names);
peaopt.idOutput = strmatch('Output', truemodel.M.endo_names);
peaopt.idConsumption = strmatch('Consumption', truemodel.M.endo_names);
peaopt.offsetparams = length(truemodel.M.params);

initialize_with_ep = true;

% Set the size of the simulated sample.
peaopt.samplesize = 10000;

% Remove the first simulations.
burnin = 1000;
peaopt.init = burnin+1;

% Simuation of the model with perturnation approach (2nd order).
truemodel.options.order = 2;
truemodel.options.periods = peaopt.samplesize+10;
check_model(truemodel.M);
truemodel.oo.dr = set_state_space(truemodel.oo.dr, truemodel.M, truemodel.options);
[truemodel.oo.dr, info, truemodel.M, truemodel.options, truemodel.oo] = resol(0, truemodel.M, truemodel.options, truemodel.oo);
set_dynare_seed(31415);
[simulatedvariables, truemodel.oo] = simult(truemodel.oo.dr.ys, truemodel.oo.dr, truemodel.M, truemodel.options, truemodel.oo);

if initialize_with_ep
    ts = extended_path([], 1000, [], truemodel.options, truemodel.M, truemodel.oo);
    ExpectedTerm = ts.ExpectedTerm.data;
    Capital = ts.Capital.data;
    Efficiency = ts.Efficiency.data;
    MCapital = mean(Capital);
    SCapital = sqrt(var(Capital));
    MEfficiency = mean(Efficiency);
    SEfficiency = sqrt(var(Efficiency));
    truemodel.oo.endo_simul = transpose(ts.data);
else
    ExpectedTerm = simulatedvariables(peaopt.idExpectedTerms, peaopt.init:peaopt.samplesize)';
    lCapital = log(simulatedvariables(peaopt.idCapital, peaopt.init-1:peaopt.samplesize-1))';
    lEfficiency = log(simulatedvariables(peaopt.idEfficiency,peaopt.init:peaopt.samplesize))';
    truemodel.oo.endo_simul = simulatedvariables;
end

% Set option for optimizer (NLS in PEA).
nlsopt = optimoptions('lsqcurvefit','Display','iter','Algorithm','levenberg-marquardt');

% Set approximation order.
k = 5;

% Define polynomial terms.
powers = build_complete_grid_powers(2, k);

Capital_ = (Capital-MCapital)/SCapital;
Efficiency_ = (Efficiency-MEfficiency)/SEfficiency;


% NLS: Estimate the PEA-2 parameters (used as a first guess) 
X = evaluate_hermite_multivariate_monomials({Capital_ Efficiency_}, powers);
old = zeros(rows(powers),1); 
old(1) = log(truemodel.oo.steady_state(peaopt.idExpectedTerms));
[rparams_init, resnorm, residual] = lsqcurvefit(@pea_predictor, old, X, ExpectedTerm, [], [], nlsopt); %%

skipline(2)
disp(['Initial guess for the PEA-' int2str(k) ' parameters '])
skipline()
for i=1:rows(powers)
disp(sprintf(' Capital^%s*Efficiency^%s \t   %2.6f',int2str(powers(i,1)),int2str(powers(i,2)),rparams_init(i)))
end
skipline()

s2old = (residual'*residual);

% Get values of the parameters.
BETA  = truemodel.M.params(strmatch('beta', truemodel.M.param_names));
ALPHA = truemodel.M.params(strmatch('alpha', truemodel.M.param_names));
TAU   = truemodel.M.params(strmatch('tau', truemodel.M.param_names));
DELTA = truemodel.M.params(strmatch('delta', truemodel.M.param_names));
RHO   = truemodel.M.params(strmatch('rho', truemodel.M.param_names));
SIGMA = truemodel.M.params(strmatch('sigma', truemodel.M.param_names));
ASTAR = truemodel.M.params(strmatch('effstar', truemodel.M.param_names));

lambda = 1;
tolerance = 1e-2; % Percentage change in the reduced form parameters.
maxiter = 50000;

% Generate productivity time series (once for all)
set_dynare_seed(31415);
efficiency  = zeros(peaopt.samplesize+1, 1);
innovations = SIGMA*randn(peaopt.samplesize+1, 1);
for t=2:peaopt.samplesize+1
    efficiency(t) = RHO*efficiency(t-1)+innovations(t);
end
Efficiency = ASTAR*exp(efficiency(2:end));
MEfficiency = mean(Efficiency);
SEfficiency = sqrt(var(Efficiency));
Efficiency_ = (Efficiency-MEfficiency)/SEfficiency;
efficiency = efficiency(2:end);

crits = Inf(5,1);
RPARAMS = NaN(length(rparams_init), 2);

RPARAMS(:,1) = rparams_init(:); 

Tf = peaopt.init:peaopt.samplesize;
T0 = Tf-1;

% Set option for optimizer (NLS in PEA).
nlsopt = optimoptions('lsqcurvefit','Display','off');%,'Algorithm','levenberg-marquardt');


% Main loop ()
iteration = 1;
while 1
    Capital_ = zeros(peaopt.samplesize, 1);
    Capital = zeros(peaopt.samplesize+1, 1);
    Consumption = zeros(peaopt.samplesize, 1);
    Investment = zeros(peaopt.samplesize, 1);
    Output = zeros(peaopt.samplesize, 1);
    Capital(1) = truemodel.oo.steady_state(peaopt.idCapital);
    ExpectedTerm = zeros(peaopt.samplesize, 1);
    X = zeros(peaopt.samplesize, rows(powers));
    for t=1:peaopt.samplesize
        Capital_(t) = (Capital(t)-MCapital)/SCapital;
        X(t, :) = evaluate_hermite_multivariate_monomials({Capital_(t) Efficiency_(t)}, powers);
        ExpectedTerm(t) = exp(X(t,:)*RPARAMS(:, 1));
        Output(t) = Efficiency(t)*Capital(t)^ALPHA;
        Consumption(t) = ExpectedTerm(t)^(-1/TAU);
        Investment(t) = Output(t)-Consumption(t);
        Capital(t+1) = (1-DELTA)*Capital(t)+Investment(t);
    end
    E = BETA*ExpectedTerm(Tf).*(ALPHA*Efficiency(Tf).*Capital(Tf).^(ALPHA-1)+1-DELTA);
    %MCapital = mean(Capital);
    %SCapital = sqrt(var(Capital));
    [rparams0, resnorm0,residual0,exitflag0] = lsqcurvefit(@pea_predictor, RPARAMS(:, 1), X(T0,:), E, [], [], nlsopt);%
    s2 = residual0'*residual0;
    RPARAMS(:,2) = RPARAMS(:,1);
    RPARAMS(:,1) = lambda*rparams0 + (1-lambda)*RPARAMS(:,1); 
    crit = 100*max(abs((RPARAMS(:,1)-RPARAMS(:,2))./RPARAMS(:,2)));
    ds2 = 100*(s2-s2old)/s2old;
    s2old = s2;
    iteration = iteration + 1;
    disp(sprintf('Iteration %s\t crit = %3.16f\t s2 = %1.8f\t D(s2) = %1.6f',int2str(iteration),crit,s2,ds2))
    crits(1:end-1) = crits(2:end);
    crits(end) = crit;
    if all(crits<tolerance)
        exitflag0
        break
    end
    if iteration>maxiter
        break
    end
end

rparams = RPARAMS(:,1);

skipline()
for i=1:rows(powers)
disp(sprintf(' Capital^%s*Efficiency^%s \t   %2.6f',int2str(powers(i,1)),int2str(powers(i,2)),rparams(i)))
end
skipline()

data = [Capital(1:end-1), Output, Consumption, Efficiency, efficiency, ExpectedTerm]';

save(['rbc.pea' int2str(k) '.mat'],'Output','Investment','Consumption','Capital','Efficiency','efficiency','ExpectedTerm','innovations','rparams','peaopt','truemodel','data');
clearvars