var Capital, Output, Consumption, Efficiency, efficiency, ExpectedTerm;

varexo EfficiencyInnovation;

parameters beta, tau, alpha, delta, rho, effstar, sigma;

beta    =  0.960;
tau     =  2.000;
alpha   =  0.330;
delta   =  0.100;
rho     =  0.900;
effstar =  1.000;
sigma   =  0.013;

model(use_dll);

  // Eq. 1
  efficiency = rho*efficiency(-1)+sigma*EfficiencyInnovation;

  // Eq. 2
  Efficiency = effstar*exp(efficiency);

  // Eq. 3 (Production function)
  Output = Efficiency*Capital(-1)^alpha;

  // Eq. 4 (Transition equation)
  Consumption + Capital - Output - (1-delta)*Capital(-1);

  // Eq. 5 (Euler equation)
  Consumption^(-tau) - ExpectedTerm;

  // Eq. n°6:
  ExpectedTerm = beta*Consumption(1)^(-tau)*(alpha*Output(1)/Capital+1-delta);

end;

shocks;
var EfficiencyInnovation = 1;
end;

steady_state_model;
  efficiency = 0;
  Efficiency = effstar;
  Capital = (alpha*effstar/(delta+1/beta-1))^(1/(1-alpha));
  Output = effstar*Capital^alpha;
  Consumption = Output-delta*Capital;
  ExpectedTerm = Consumption^(-tau);
end;

steady;