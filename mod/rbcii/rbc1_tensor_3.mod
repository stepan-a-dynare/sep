@#include "rbc.inc"

copyfile('rbc_steady_state.m','rbc1_tensor_3_steadystate2.m');

steady;

options_.ep.stochastic.order = 1;

set_dynare_seed('default');
ts = extended_path([], 2000, [], options_, M_, oo_);

save('sep_1_tensor_3.mat','ts')
