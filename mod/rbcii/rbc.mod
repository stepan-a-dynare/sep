var Capital, Output, Labour, Consumption,  Investment, Output1, Labour1, Consumption1, Output2, Labour2, Consumption2, Efficiency, efficiency, ExpectedTerm, LagrangeMultiplier;

varexo EfficiencyInnovation;

parameters beta, theta, tau, alpha, psi, delta, rho, effstar, sigma;

/*
** Calibration
*/

beta    =  0.990;
theta   =  0.357;
tau     =  2.000;
alpha   =  0.450;
psi     = -0.200;
delta   =  0.020;
rho     =  0.800;
effstar =  1.000;
sigma   =  0.100;

model(use_dll);

  efficiency = rho*efficiency(-1) + sigma*EfficiencyInnovation;

  Efficiency = effstar*exp(efficiency);

  (((Consumption1^theta)*((1-Labour1)^(1-theta)))^(1-tau))/Consumption1 -LagrangeMultiplier - beta*ExpectedTerm(1) + LagrangeMultiplier(1)*beta*(1-delta);

  ExpectedTerm = ((((Consumption^theta)*((1-Labour)^(1-theta)))^(1-tau))/Consumption)*(alpha*((Output/Capital(-1))^(1-psi))+1-delta);

  LagrangeMultiplier = max((((Consumption^theta)*((1-Labour)^(1-theta)))^(1-tau))/Consumption - beta*ExpectedTerm(1) + LagrangeMultiplier(1)*beta*(1-delta),0);

  ((1-theta)/theta)*(Consumption1/(1-Labour1)) - (1-alpha)*(Output1/Labour1)^(1-psi);

  Output1 = Efficiency*(alpha*(Capital(-1)^psi)+(1-alpha)*(Labour1^psi))^(1/psi);

  Consumption2  = Output2;

  ((1-theta)/theta)*(Consumption2/(1-Labour2)) - (1-alpha)*(Output2/Labour2)^(1-psi);

  Output2 = Efficiency*(alpha*(Capital(-1)^psi)+(1-alpha)*(Labour2^psi))^(1/psi);

  Consumption = (Output1 > Consumption1)*Consumption1 + (1-(Output1 >= Consumption1))*Consumption2;

  Labour = (Output1 > Consumption1)*Labour1 + (1-(Output1 >= Consumption1))*Labour2;

  Output = (Output1 > Consumption1)*Output1 + (1-(Output1 >= Consumption1))*Output2;

  Capital = Output-Consumption + (1-delta)*Capital(-1);

  Investment = Capital - (1-delta)*Capital(-1);

end;

// Write analytical steady state file (without globals)
options_.steadystate_flag = 2;
copyfile('rbc_steady_state.m','rbc_steadystate2.m');

shocks;
var EfficiencyInnovation = 1;
end;

steady(nocheck);

//options_.ep.stochastic.algo=1;

set_dynare_seed('default');
ts0 = extended_path(oo_.steady_state, 100, [], options_, M_, oo_);

    //options_.simul.maxit = 100;
    //options_.ep.verbosity = 0;
    //options_.ep.stochastic.order = 0;
    //options_.ep.stochastic.nodes = 3;
    //options_.console_mode = 0;


    options_.ep.stochastic.order = 1;
    options_.ep.IntegrationAlgorithm='Tensor-Gaussian-Quadrature';
	set_dynare_seed('default'); 
   ts1 = extended_path([], 100, [], options_, M_, oo_);
    options_.ep.stochastic.order = 2;
    	set_dynare_seed('default');
	ts2 = extended_path([], 100, [], options_, M_, oo_);
    options_.ep.stochastic.order = 3;
	set_dynare_seed('default'); 
   ts3 = extended_path([], 100, [], options_, M_, oo_);
