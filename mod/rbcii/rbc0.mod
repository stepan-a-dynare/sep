@#include "rbc.inc"

options_.steadystate_flag = 2;
copyfile('rbc_steady_state.m','rbc0_steadystate2.m');

options_.ep.solve_algo=9;
options_.solve_tolf = 1e-8;

steady;

set_dynare_seed('default');
ts = extended_path([], 2000, [], options_, M_, oo_);

save('ep.mat','ts')
