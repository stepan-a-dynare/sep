@#include "rbc.inc"

copyfile('rbc_steady_state.m','rbc4_tensor_3_steadystate2.m');

steady;

options_.ep.stochastic.order = 4;

set_dynare_seed('default');
ts = extended_path([], 2000, [], options_, M_, oo_);

save('sep_4_tensor_3.mat','ts')
