  // Eq. 1
  efficiency = rho*efficiency(-1) + sigma*EfficiencyInnovation;

  // Eq. 2
  Efficiency = effstar*exp(efficiency);//-.5*sigma*sigma/(1-rho*rho));

  // Eq. 3
  Output = Efficiency*(alpha*(Capital(-1)^psi)+(1-alpha)*(Labour^psi))^(1/psi);

  // Eq. 4
  Consumption + Capital - Output - (1-delta)*Capital(-1);

  // Eq. 5
  ((1-theta)/theta)*(Consumption/(1-Labour)) - (1-alpha)*(Output/Labour)^(1-psi);

  // Eq. 6
  (((Consumption^theta)*((1-Labour)^(1-theta)))^(1-tau))/Consumption - ExpectedTerm;
