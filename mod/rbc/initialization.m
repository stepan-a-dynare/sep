locals;

diary('pea.log')

% Compile the original model.
dynare('rbc.mod', 'console', 'fast','noclearall');
truemodel.oo = oo_;
truemodel.M = M_;
truemodel.options = options_;
warning off all,
delete('rbc.m', 'rbc.log', 'rbc_results.mat');
warning on all
clear('oo_', 'M_', 'options_')

% Compile the model where the expected term is approximated by a multivariate polynomial (2nd order).
dynare('rbc_pea1.mod', 'console', 'fast','noclearall');
peamodel1.oo = oo_;
peamodel1.M = M_;
peamodel1.options = options_;
warning off all
delete('rbc_pea1.m', 'rbc_pea1.log', 'rbc_pea1_results.mat');
warning on all
clear('oo_', 'M_', 'options_')

% Compile the model where the expected term is approximated by a multivariate polynomial (2nd order).
dynare('rbc_pea2.mod', 'console', 'fast','noclearall');
peamodel2.oo = oo_;
peamodel2.M = M_;
peamodel2.options = options_;
warning off all
delete('rbc_pea2.m', 'rbc_pea2.log', 'rbc_pea2_results.mat');
warning on all
clear('oo_', 'M_', 'options_')

% Define indices used for the approximation.
peaopt.idExpectedTerms = strmatch('ExpectedTerm', peamodel2.M.endo_names);
peaopt.idExpectedEquations = 7;
peaopt.idEfficiency = strmatch('Efficiency', peamodel2.M.endo_names);
peaopt.idCapital = strmatch('Capital', peamodel2.M.endo_names);

% Define dynamic model routine.
peaopt.dynamicmodel = str2func('rbc_dynamic');

% Set the size of the simulated sample.
peaopt.samplesize = 20000;

% Remove the first simulations.
burnin = 1000;
peaopt.init = burnin+1;

% Get the number of parameters in the original model, parameters in the approximated model with
% index greater than offsetparams are used for the approximation of the expected term.
peaopt.offsetparams = length(truemodel.M.params);

% Simuation of the model with perturnation approach (2nd order).
truemodel.options.order = 2;
truemodel.options.periods = peaopt.samplesize+10;
check_model(truemodel.M);
truemodel.oo.dr = set_state_space(truemodel.oo.dr, truemodel.M, truemodel.options);
[truemodel.oo.dr, info, truemodel.M, truemodel.options, truemodel.oo] = resol(0, truemodel.M, truemodel.options, truemodel.oo);
set_dynare_seed(31415);
[simulatedvariables, truemodel.oo] = simult(truemodel.oo.dr.ys, truemodel.oo.dr, truemodel.M, truemodel.options, truemodel.oo);
ExpectedTerm = simulatedvariables(peaopt.idExpectedTerms, peaopt.init:5000)';
lCapital = log(simulatedvariables(peaopt.idCapital, peaopt.init-1:5000-1))';
lEfficiency = log(simulatedvariables(peaopt.idEfficiency,peaopt.init:5000))';

% Set option for optimizer (NLS in PEA).
nlsopt = optimoptions('lsqcurvefit','Display','off');

% NLS: Estimate the PEA-1 parameters (used as a first guess) 
X1 = [ones(rows(lEfficiency), 1), lCapital, lEfficiency];
old = zeros(3,1); old(1) = log(truemodel.oo.steady_state(peaopt.idExpectedTerms));
peamodel1.M.params(peaopt.offsetparams+1:end) =  lsqcurvefit(@pea_predictor, old, X1, ExpectedTerm, [], [], nlsopt);

[BetaPEA1, peamodel1] = pea_solve_by_iterations(peamodel1, truemodel, peaopt.samplesize, 1, 1, 1000, .001, 31415, peaopt, nlsopt);

BetaPEA1

peamodel2.M.params(peaopt.offsetparams+1:end) = 0;
peamodel2.M.params(peaopt.offsetparams+1:peaopt.offsetparams+3) = BetaPEA1;

[BetaPEA2, peamodel2] = pea_solve_by_iterations(peamodel2, truemodel, peaopt.samplesize, 2, .1, 10000, .001, 31415, peaopt, nlsopt);

BetaPEA2

%
%save('initialization.mat','truemodel','peamodel2','BetaPEA2','peaopt','nlsopt','BetaPEA2');

diary off;