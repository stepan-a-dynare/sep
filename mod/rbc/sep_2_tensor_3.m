locals;

load('initialization.mat');

global options_ 
options_ = truemodel.options;

dynare_config();

skipline()
disp('    ------------------------------')
disp('      Extended Path (2-tensor-3)  ')
disp('    ------------------------------')
skipline()

truemodel.options.ep.stochastic.status = 1;
truemodel.options.ep.IntegrationAlgorithm='Tensor-Gaussian-Quadrature';
truemodel.options.ep.stochastic.order = 2;
truemodel.options.ep.quadrature.nodes = 3;

ts2_tensor_3 = extended_path(truemodel.oo.steady_state, peaopt.samplesize, peamodel2.oo.exo_simul, truemodel.options, truemodel.M, truemodel.oo);

Z = log([ts2_tensor_3.Capital.data(peaopt.init-1:peaopt.samplesize-1), ts2_tensor_3.Efficiency.data(peaopt.init:peaopt.samplesize)]);
ExpectedTerm = ts2_tensor_3.ExpectedTerm.data(peaopt.init:peaopt.samplesize);
compare_pea_and_ep;

save('sep_2_tensor_3.mat','ts2_tensor_3','Beta2');