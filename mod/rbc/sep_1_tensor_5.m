locals;

load('initialization.mat');

global options_ 
options_ = truemodel.options;

dynare_config();

skipline()
disp('    ------------------------------')
disp('      Extended Path (1-tensor-5)  ')
disp('    ------------------------------')
skipline()

truemodel.options.ep.stochastic.status = 1;
truemodel.options.ep.IntegrationAlgorithm='Tensor-Gaussian-Quadrature';
truemodel.options.ep.stochastic.order = 1;
truemodel.options.ep.quadrature.nodes = 5;

ts1_tensor_5 = extended_path(truemodel.oo.steady_state, peaopt.samplesize, peamodel2.oo.exo_simul, truemodel.options, truemodel.M, truemodel.oo);

Z = log([ts1_tensor_5.Capital.data(peaopt.init-1:peaopt.samplesize-1), ts1_tensor_5.Efficiency.data(peaopt.init:peaopt.samplesize)]);
ExpectedTerm = ts1_tensor_5.ExpectedTerm.data(peaopt.init:peaopt.samplesize);
compare_pea_and_ep;

save('sep_1_tensor_5.mat','ts1_tensor_5','Beta2');