@#includepath "./common"

@#include "header.inc"

parameters t0,
	   t1_k, t1_a,
	   t2_kk, t2_ka, t2_aa,
	   t3_kkk, t3_kka, t3_kaa, t3_aaa;

@#include "calibration.inc"

t0 = 1;
t1_k = .1;
t1_a = .2;
t2_kk = 0;
t2_aa = 0;
t2_ka = 0;
t3_kkk = 0;
t3_kka = 0;
t3_kaa = 0;
t3_aaa = 0;

model(use_dll);

  @#include "model.inc"

  // Eq. n°7:
  ExpectedTerm = exp(t0 + t1_k*log(Capital(-1)) + t1_a*log(Efficiency)
  	       	 	+ t2_kk*log(Capital(-1))*log(Capital(-1))  + t2_ka*log(Capital(-1))*log(Efficiency)) + t2_aa*log(Efficiency)*log(Efficiency)
			+ t3_kkk*log(Capital(-1))*log(Capital(-1))*log(Capital(-1)) + t3_kka*log(Capital(-1))*log(Capital(-1))*log(Efficiency)
			  							    + t3_kaa*log(Capital(-1))*log(Efficiency)*log(Efficiency)
										    + t3_aaa*log(Efficiency)*log(Efficiency)*log(Efficiency);
end;

@#include "shocks.inc" 