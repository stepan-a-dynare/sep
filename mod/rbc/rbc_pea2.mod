@#includepath "./common"

@#include "header.inc"

parameters t0,
	   t1_k, t1_a,
	   t2_kk, t2_ka, t2_aa;

@#include "calibration.inc"

t0 = 1;
t1_k = .1;
t1_a = .2;
t2_kk = 0;
t2_aa = 0;
t2_ka = 0;

model(use_dll);

  @#include "model.inc"

  // Eq. n°7:
  ExpectedTerm = exp(t0 + t1_k*log(Capital(-1)) + t1_a*log(Efficiency)
  	       	 	+ t2_kk*log(Capital(-1))*log(Capital(-1)) + t2_ka*log(Capital(-1))*log(Efficiency) + t2_aa*log(Efficiency)*log(Efficiency));
end;

@#include "shocks.inc"
