@#includepath "./common"

@#include "header.inc"

parameters t0,
	   t1_k, t1_a ;

@#include "calibration.inc"

t0 = 1;
t1_k = .1;
t1_a = .2;

model(use_dll);

  @#include "model.inc"

  // Eq. n°7:
  ExpectedTerm = exp(t0 + t1_k*log(Capital(-1)) + t1_a*log(Efficiency));

end;

@#include "shocks.inc"
