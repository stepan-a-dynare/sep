locals;

load('initialization.mat');

global options_ 
options_ = truemodel.options;

dynare_config();

skipline()
disp('    ----------------------------')
disp('      Extended Path (3-UT_2p+1) ')
disp('    ----------------------------')
skipline()

truemodel.options.ep.stochastic.status = 1;
truemodel.options.ep.IntegrationAlgorithm='UT_2p+1';
truemodel.options.ep.stochastic.order = 3;
truemodel.options.ep.ut.k = 0;

ts3_unscented = extended_path(truemodel.oo.steady_state, peaopt.samplesize, peamodel2.oo.exo_simul, truemodel.options, truemodel.M, truemodel.oo);

Z = log([ts3_unscented.Capital.data(peaopt.init-1:peaopt.samplesize-1), ts3_unscented.Efficiency.data(peaopt.init:peaopt.samplesize)]);
ExpectedTerm = ts3_unscented.ExpectedTerm.data(peaopt.init:peaopt.samplesize);
compare_pea_and_ep;

save('sep_3_unscented.mat','ts3_unscented','Beta2');
