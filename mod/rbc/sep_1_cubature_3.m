locals;

load('initialization.mat');

global options_ 
options_ = truemodel.options;

dynare_config();

skipline()
disp('    --------------------------------')
disp('      Extended Path (1-cubature-3)  ')
disp('    --------------------------------')
skipline()

truemodel.options.ep.stochastic.status = 1;
truemodel.options.ep.IntegrationAlgorithm='Stroud-Cubature-3';
truemodel.options.ep.stochastic.order = 1;

ts1_cubature_3 = extended_path(truemodel.oo.steady_state, peaopt.samplesize, peamodel2.oo.exo_simul, truemodel.options, truemodel.M, truemodel.oo);

Z = log([ts1_cubature_3.Capital.data(peaopt.init-1:peaopt.samplesize-1), ts1_cubature_3.Efficiency.data(peaopt.init:peaopt.samplesize)]);
ExpectedTerm = ts1_cubature_3.ExpectedTerm.data(peaopt.init:peaopt.samplesize);
compare_pea_and_ep;

save('sep_1_cubature_3.mat','ts1_cubature_3','Beta2');
