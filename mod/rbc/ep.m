locals;

dynare_config();

load('initialization.mat');

global options_ 
options_ = truemodel.options;

skipline()
disp('    ---------------------')
disp('      Extended Path (0)  ')
disp('    ---------------------')
skipline()

ts0 = extended_path(truemodel.oo.steady_state, peaopt.samplesize, peamodel2.oo.exo_simul, truemodel.options, truemodel.M, truemodel.oo);

Z = log([ts0.Capital.data(peaopt.init-1:peaopt.samplesize-1), ts0.Efficiency.data(peaopt.init:peaopt.samplesize)]);
ExpectedTerm = ts0.ExpectedTerm.data(peaopt.init:peaopt.samplesize);
compare_pea_and_ep;

save('ep.mat','ts0','Beta2');