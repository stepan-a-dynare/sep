locals;

load('initialization.mat');

global options_ 
options_ = truemodel.options;

dynare_config();

skipline()
disp('    ----------------------------')
disp('      Extended Path (5-UT_2p+1) ')
disp('    ----------------------------')
skipline()

truemodel.options.ep.stochastic.status = 1;
truemodel.options.ep.IntegrationAlgorithm='UT_2p+1';
truemodel.options.ep.stochastic.order = 5;
truemodel.options.ep.ut.k = 0;

ts5_unscented = extended_path(truemodel.oo.steady_state, peaopt.samplesize, peamodel2.oo.exo_simul, truemodel.options, truemodel.M, truemodel.oo);

Z = log([ts5_unscented.Capital.data(peaopt.init-1:peaopt.samplesize-1), ts5_unscented.Efficiency.data(peaopt.init:peaopt.samplesize)]);
ExpectedTerm = ts5_unscented.ExpectedTerm.data(peaopt.init:peaopt.samplesize);
compare_pea_and_ep;

save('sep_5_unscented.mat','ts5_unscented','Beta2');
