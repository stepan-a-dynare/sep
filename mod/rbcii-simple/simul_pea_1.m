locals;
dynare_config();

% Compile the original model.
dynare('rbc.mod', 'console','noclearall');
truemodel.oo = oo_;
truemodel.M = M_;
truemodel.options = options_;
warning off all,
delete('rbc.m', 'rbc.log', 'rbc_results.mat');
warning on all
clear('oo_', 'M_', 'options_')

% Define indices used for the approximation.
peaopt.idExpectedTerms = strmatch('ExpectedTerm', truemodel.M.endo_names);
peaopt.idExpectedEquations = 6;
peaopt.idEfficiency = strmatch('Efficiency', truemodel.M.endo_names);
peaopt.idCapital = strmatch('Capital', truemodel.M.endo_names);
peaopt.offsetparams = length(truemodel.M.params);

% Set the size of the simulated sample.
peaopt.samplesize = 100000;

% Remove the first simulations.
burnin = 1000;
peaopt.init = burnin+1;

% Simuation of the model with perturnation approach (2nd order).
truemodel.options.order = 2;
truemodel.options.periods = peaopt.samplesize+10;
check_model(truemodel.M);
truemodel.oo.dr = set_state_space(truemodel.oo.dr, truemodel.M, truemodel.options);
[truemodel.oo.dr, info, truemodel.M, truemodel.options, truemodel.oo] = resol(0, truemodel.M, truemodel.options, truemodel.oo);
set_dynare_seed(31415);
[simulatedvariables, truemodel.oo] = simult(truemodel.oo.dr.ys, truemodel.oo.dr, truemodel.M, truemodel.options, truemodel.oo);
ExpectedTerm = simulatedvariables(peaopt.idExpectedTerms, peaopt.init:peaopt.samplesize)';
lCapital = log(simulatedvariables(peaopt.idCapital, peaopt.init-1:peaopt.samplesize-1))';
lEfficiency = log(simulatedvariables(peaopt.idEfficiency,peaopt.init:peaopt.samplesize))';
truemodel.oo.endo_simul = simulatedvariables;

% Set option for optimizer (NLS in PEA).
nlsopt = optimoptions('lsqcurvefit','Display','off');

% NLS: Estimate the PEA-2 parameters (used as a first guess) 
X1 = [ones(rows(lEfficiency), 1), lCapital, lEfficiency];
old = zeros(3,1); 
old(1) = log(truemodel.oo.steady_state(peaopt.idExpectedTerms));
[rparams, resnorm, residual] = lsqcurvefit(@pea_predictor, old, X1, ExpectedTerm, [], [], nlsopt); %%

skipline(2)
disp('Initial guess for the PEA-2 parameters ')
skipline()
disp(sprintf(' Constant                %2.6f',rparams(1)))
disp(sprintf(' Capital                 %2.6f',rparams(2)))
disp(sprintf(' Efficiency              %2.6f',rparams(3)))
skipline()

s2old = (residual'*residual);

% Get values of the parameters.
BETA  = truemodel.M.params(strmatch('beta', truemodel.M.param_names));
ALPHA = truemodel.M.params(strmatch('alpha', truemodel.M.param_names));
TAU   = truemodel.M.params(strmatch('tau', truemodel.M.param_names));
DELTA = truemodel.M.params(strmatch('delta', truemodel.M.param_names));
RHO   = truemodel.M.params(strmatch('rho', truemodel.M.param_names));
SIGMA = truemodel.M.params(strmatch('sigma', truemodel.M.param_names));
ASTAR = truemodel.M.params(strmatch('effstar', truemodel.M.param_names));
ISS   = truemodel.oo.steady_state(strmatch('Investment', truemodel.M.endo_names));

lambda = 1;
tolerance = 1e-4; % Percentage change in the reduced form parameters.
maxiter = 50000;

% Generate productivity time series (once for all)
set_dynare_seed(31415);
efficiency  = zeros(peaopt.samplesize, 1);
innovations = [0; SIGMA*randn(peaopt.samplesize-1, 1)];
for t=2:peaopt.samplesize
    efficiency(t) = RHO*efficiency(t-1)+innovations(t);
end
Efficiency = ASTAR*exp(efficiency);

% Set options for NLS
nlsopt = optimoptions('lsqcurvefit','Display','off','Algorithm','levenberg-marquardt');

crits = Inf(10,1);
RPARAMS = NaN(3, 2);
RPARAMS(:,1) = rparams; 

% Main loop ()
iteration = 1;
while 1
    Capital = zeros(peaopt.samplesize+1, 1); Capital(1) = truemodel.oo.steady_state(peaopt.idCapital);
    ExpectedTerm = zeros(peaopt.samplesize, 1);
    LagrangeMultiplier = zeros(peaopt.samplesize, 1);
    X1 = zeros(peaopt.samplesize, 3);
    for t=1:peaopt.samplesize
        X1(t, :) = [1, log(Capital(t)), log(Efficiency(t))];
        ExpectedTerm(t) = exp(X1(t,:)*RPARAMS(:, 1));
        Output = Efficiency(t)*Capital(t)^ALPHA;
        Consumption = ExpectedTerm(t)^(-1/TAU);
        Investment = Output - Consumption;
        if Investment>.975*ISS
            Capital(t+1) = Output+(1-DELTA)*Capital(t)-Consumption;
            LagrangeMultiplier(t) = 0;
        else
            Capital(t+1) = (1-DELTA)*Capital(t)+.975*ISS;
            LagrangeMultiplier(t) = Consumption^(-TAU)-ExpectedTerm(t);
        end
    end
    ExpectedTerm_ = beta*ExpectedTerm(2:peaopt.samplesize).*(ALPHA*Efficiency(2:peaopt.samplesize).*Capital(2:peaopt.samplesize).^(ALPHA-1)+1-DELTA-LagrangeMultiplier(2:peaopt.samplesize)*(1-DELTA));
    [rparams0, resnorm0,residual0,exitflag0] = lsqcurvefit(@pea_predictor, RPARAMS(:, 1), X1(1:peaopt.samplesize-1,:), ExpectedTerm_, [], [], nlsopt);
    s2 = residual0'*residual0;
    RPARAMS(:,2) = RPARAMS(:,1);
    RPARAMS(:,1) = lambda*rparams0 + (1-lambda)*RPARAMS(:,1); 
    crit = 100*max(abs(RPARAMS(:,1)-RPARAMS(:,2))./abs(RPARAMS(:,2)));
    ds2 = 100*(s2-s2old)/s2old;
    s2old = s2;
    iteration = iteration + 1;
    disp(sprintf('Iteration %s\t crit = %3.16f\t s2 = %1.8f\t D(s2) = %1.6f',int2str(iteration),crit,s2,ds2))
    crits(1:end-1) = crits(2:end);
    crits(end) = crit;
    if all(crits<tolerance)
        break
    end
    if iteration>maxiter
        break
    end
end

Output  = Efficiency.*Capital(1:end-1).^ALPHA;
Investment  = Capital(2:end)-(1-DELTA)*Capital(1:end-1);
Consumption = Output-Investment;

save('rbc.pea1.mat','Output','Investment','Consumption','Capital','Efficiency','efficiency','innovations','rparams','peaopt','truemodel');
clearvars