locals;

dynare('rbc.mod', 'console','noclearall');

warning off all,
delete('rbc.m', 'rbc.log', 'rbc_results.mat');
warning on all

skipline()
disp('    ---------------------')
disp('      Extended Path (0)  ')
disp('    ---------------------')
skipline()

load('rbc.pea2.mat')
data = data(:,1:5001);

[ts0, oo_] = extended_path(oo_.steady_state, 100, innovations(2:end)/sigma, options_, M_, oo_);

figure(1)
plot(ts0.Output.data(1:1000)-ts0.Consumption.data(1:1000),'-k')
title('Investment (EP vs PEA)')
hold on, plot(Investment(1:1000),'--r'), hold off
matlab2tikz('./results/ep_versus_pea2_Investment.tex');

figure(2)
plot(ts0.Capital.data(1:1000),ts0.Capital.data(2:1001),'ok')
title('Transition (EP vs PEA)')
hold on, plot(Capital(1:1000),Capital(2:1001),'or'), hold off
matlab2tikz('./results/ep_versus_pea2_transition.tex');

figure(3)
plot(ts0.Efficiency.data(1:1000),ts0.Capital.data(1:1000),'ok')
title('States distribution (EP vs PEA)')
hold on, plot(Efficiency(1:1000),Capital(1:1000),'or'), hold off
matlab2tikz('./results/ep_versus_pea2_states.tex');

% Get the residual of the euler equation with EP and PEA
r_ep  = evaluate_dynamic_model(str2func('rbc_dynamic'), oo_.endo_simul(:), oo_.exo_simul, M_.params, oo_.steady_state, M_.lead_lag_incidence, 5000-1);
r_pea = evaluate_dynamic_model(str2func('rbc_dynamic'), data(:), oo_.exo_simul, M_.params, oo_.steady_state, M_.lead_lag_incidence, 5000-1);
euler_residual_ep = r_ep(5,:)';
euler_residual_pea = r_pea(5,:)';

figure(4)
plot(euler_residual_ep)
title('Euler residuals (EP)');
matlab2tikz('./results/ep_euler_residuals.tex');

figure(5)
plot(euler_residual_pea)
title('Euler residuals (PEA)')
matlab2tikz('./results/pea_euler_residuals.tex');

% Compute DHM statistic for EP and PEA
s_ep = [oo_.endo_simul(1,1000:4000)', oo_.endo_simul(4,1001:4001)' oo_.endo_simul(1,1000-1:4000-1)', oo_.endo_simul(4,1001-1:4001-1)'];
s_pea = [data(1,1000:4000)', data(4,1001:4001)',data(1,1000-1:4000-1)', data(4,1001-1:4001-1)'];
h_ep  = [ones(rows(s_ep), 1), s_ep, s_ep.^2, prod(s_ep(:,1:2),2), prod(s_ep(:,3:4),2), prod(s_ep(:,[1,3]),2), prod(s_ep(:,[1,4]),2), prod(s_ep(:,[2,4]),2)];
h_pea = [ones(rows(s_pea), 1), s_pea, s_pea.^2, prod(s_pea(:,1:2),2), prod(s_pea(:,3:4),2), prod(s_pea(:,[1,3]),2), prod(s_pea(:,[1,4]),2), prod(s_pea(:,[2,4]),2)];
euler_residual_ep_ = euler_residual_ep(1001:4001);
euler_residual_pea_ = euler_residual_pea(1001:4001);
tmp_ep  = bsxfun(@times, h_ep, euler_residual_ep_);
tmp_pea = bsxfun(@times, h_pea, euler_residual_pea_);
m_ep  = mean(tmp_ep)';
m_pea = mean(tmp_pea)';
w_ep  = long_run_variance(tmp_ep);
w_pea = long_run_variance(tmp_pea);
T = length(m_ep);
Jep = T*m_ep'*inv(w_ep)*m_ep;
Jpea = T*m_pea'*inv(w_ep)*m_pea;
pvalue_ep = chi2cdf(Jep,columns(h_ep),'upper');
pvalue_pea = chi2cdf(Jpea,columns(h_pea),'upper');
