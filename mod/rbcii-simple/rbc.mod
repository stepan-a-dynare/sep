var Capital, Output, Consumption, Investment, Efficiency, efficiency, ExpectedTerm, LagrangeMultiplier, Consumption1, Consumption2;

varexo EfficiencyInnovation;

parameters beta, tau, alpha, delta, rho, effstar, sigma;

@#define scale = ".975"

@#include "calibration.inc"

model(use_dll);

  // Eq. 1
  efficiency = rho*efficiency(-1)+sigma*EfficiencyInnovation;

  // Eq. 2
  Efficiency = effstar*exp(efficiency);

  // Eq. 3 (Production function)
  Output = Efficiency*Capital(-1)^alpha;

  // Eq. 4 (Transition equation)
  Capital = Investment + (1-delta)*Capital(-1);

  // Eq. 5 (Euler equation, unconstrained regime)
  Consumption1^(-tau) - ExpectedTerm;

  // Eq. 6
  Consumption2 = Output-@{scale}*STEADY_STATE(Investment);

  // Eq. 7
  Consumption = (Output>(Consumption1+@{scale}*STEADY_STATE(Investment)))*Consumption1 + (1-(Output>(Consumption1+@{scale}*STEADY_STATE(Investment))))*Consumption2;

  // Eq. 8 (Lagrange multiplier associated to the posoitivity constraint)
  LagrangeMultiplier = max(Consumption^(-tau) - ExpectedTerm, 0);

  // Eq. 9
  Investment = max(Output-Consumption, @{scale}*STEADY_STATE(Investment));

  // Eq. 10:
  ExpectedTerm = beta*Consumption(1)^(-tau)*(alpha*Output(1)/Capital+1-delta)-beta*LagrangeMultiplier(1)*(1-delta);

end;

shocks;
var EfficiencyInnovation = 1;
end;

steady_state_model;
  efficiency = 0;
  Efficiency = effstar;
  Capital = (alpha*effstar/(delta+1/beta-1))^(1/(1-alpha));
  Output = effstar*Capital^alpha;
  Consumption = Output-delta*Capital;
  Consumption1 = Consumption;
  Investment = Output-Consumption;
  Consumption2 = Output-@{scale}*Investment;
  ExpectedTerm = Consumption^(-tau);
  LagrangeMultiplier = 0;
end;

steady;