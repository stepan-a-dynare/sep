var Capital, Output, Consumption, Investment, Efficiency, efficiency, ExpectedTerm, LagrangeMultiplier, Consumption1, Consumption2;

varexo EfficiencyInnovation;

parameters beta, tau, alpha, delta, rho, effstar, sigma;

@#define scale = ".975"

@#include "calibration.inc"

model(use_dll);

  // Eq. 1
  efficiency = rho*efficiency(-1)+sigma*EfficiencyInnovation;

  // Eq. 2
  Efficiency = effstar*exp(efficiency);

  // Eq. 3 (Production function)
  Output = Efficiency*Capital(-1)^alpha;

  // Eq. 4 (Transition equation)
  Capital = Investment + (1-delta)*Capital(-1);

  // Eq. 5 (Euler equation, unconstrained regime)
  Consumption1^(-tau) - ExpectedTerm;

  // Eq. 6
  Consumption2 = Output-@{scale}*STEADY_STATE(Investment);

  // Eq. 7
  Consumption = (Output>(Consumption1+@{scale}*STEADY_STATE(Investment)))*Consumption1 + (1-(Output>(Consumption1+@{scale}*STEADY_STATE(Investment))))*Consumption2;

  // Eq. 8 (Lagrange multiplier associated to the posoitivity constraint)
  LagrangeMultiplier = max(Consumption^(-tau) - ExpectedTerm, 0);

  // Eq. 9
  Investment = max(Output-Consumption, @{scale}*STEADY_STATE(Investment));

  // Eq. 10:
  ExpectedTerm = beta*Consumption(1)^(-tau)*(alpha*Output(1)/Capital+1-delta)-beta*LagrangeMultiplier(1)*(1-delta);

end;

shocks;
var EfficiencyInnovation = 1;
end;

steady_state_model;
  efficiency = 0;
  Efficiency = effstar;
  Capital = (alpha*effstar/(delta+1/beta-1))^(1/(1-alpha));
  Output = effstar*Capital^alpha;
  Consumption = Output-delta*Capital;
  Consumption1 = Consumption;
  Investment = Output-Consumption;
  Consumption2 = Output-@{scale}*Investment;
  ExpectedTerm = Consumption^(-tau);
  LagrangeMultiplier = 0;
end;

steady;


set_dynare_seed(31415);
innovations = randn(100000, 1);

tensor = true;
cubature5 = false;
unscented = false;

if tensor
   ts0 = extended_path([], 1000, innovations, options_, M_, oo_);

   options_.ep.stochastic.status = 1;
   options_.ep.IntegrationAlgorithm = 'Tensor-Gaussian-Quadrature';
   options_.ep.stochastic.order = 1;
   options_.ep.quadrature.nodes = 5;
   ts1_tensor_5 = extended_path([], 200, innovations, options_, M_, oo_);

   options_.ep.stochastic.status = 1;
   options_.ep.IntegrationAlgorithm = 'Tensor-Gaussian-Quadrature';
   options_.ep.stochastic.order = 2;
   options_.ep.quadrature.nodes = 5;
   ts2_tensor_5 = extended_path([], 200, innovations, options_, M_, oo_);

   options_.ep.stochastic.status = 1;
   options_.ep.IntegrationAlgorithm = 'Tensor-Gaussian-Quadrature';
   options_.ep.stochastic.order = 3;
   options_.ep.quadrature.nodes = 5;
   ts3_tensor_5 = extended_path([], 200, innovations, options_, M_, oo_);

   save('sep_tensor_5.mat','ts0','ts1_tensor_5','ts2_tensor_5','ts3_tensor_5')
end


if cubature5
   ts0 = extended_path([], 1000, innovations, options_, M_, oo_);
   options_.ep.stochastic.status = 1;
   options_.ep.IntegrationAlgorithm = 'Stroud-Cubature-5';
   options_.ep.stochastic.order = 1;
   ts1_cubature_5 = extended_path([], 200, innovations, options_, M_, oo_);
   options_.ep.stochastic.status = 1;
   options_.ep.IntegrationAlgorithm = 'Stroud-Cubature-5';
   options_.ep.stochastic.order = 2;
   ts2_cubature_5 = extended_path([], 200, innovations, options_, M_, oo_);
   options_.ep.stochastic.status = 1;
   options_.ep.IntegrationAlgorithm = 'Stroud-Cubature-5';
   options_.ep.stochastic.order = 3;
   ts3_cubature_5 = extended_path([], 200, innovations, options_, M_, oo_);
   save('sep_cubature_5.mat','ts0','ts1_cubature_5','ts2_cubature_5','ts3_cubature_5')
end

if unscented
   options_.ep.ut.k = 0;
   disp('ts0')
   ts0 = extended_path([], 1000, innovations, options_, M_, oo_);
   options_.ep.stochastic.status = 1;
   options_.ep.IntegrationAlgorithm = 'Unscented';
   options_.ep.stochastic.order = 1;
   disp('ts1')
   ts1_unscented = extended_path([], 200, innovations, options_, M_, oo_);
   options_.ep.stochastic.order = 2;
   disp('ts2')
   ts2_unscented = extended_path([], 200, innovations, options_, M_, oo_);
   options_.ep.stochastic.order = 3;
   disp('ts3')
   ts3_unscented = extended_path([], 200, innovations, options_, M_, oo_);
   options_.ep.stochastic.order = 4;
   disp('ts4')
   ts4_unscented = extended_path([], 200, innovations, options_, M_, oo_);
   options_.ep.stochastic.order = 5;
   disp('ts5')
   ts5_unscented = extended_path([], 200, innovations, options_, M_, oo_);
   options_.ep.stochastic.order = 10;
   disp('ts10')
   ts10_unscented = extended_path([], 200, innovations, options_, M_, oo_);
   save('sep_unscented.mat','ts0','ts1_unscented','ts2_unscented','ts3_unscented','ts4_unscented','ts5_unscented','ts10_unscented')
end