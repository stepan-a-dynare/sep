function ts = sep(order, nodes, method)

locals;

global oo_ options_ M_

dynare('rbc.mod', 'console','noclearall');

warning off all,
delete('rbc.m', 'rbc.log', 'rbc_results.mat');
warning on all

skipline()
disp('    ---------------------')
disp(sprintf('      Extended Path (%s)  ',int2str(order)))
disp('    ---------------------')
skipline()

load('rbc.pea2.mat')

options_ = evalin('base', 'options_');
M_ = evalin('base', 'M_');
oo_ = evalin('base', 'oo_');
sigma = evalin('base', 'sigma');

options_.dynatol.f = 1e-7;
options_.dynatol.x = 1e-7;
options_.ep.stochastic.status = 1;
options_.ep.IntegrationAlgorithm = method; % 'Tensor-Gaussian-Quadrature', 'Stroud-Cubature-3', 'Stroud-Cubature-5', 'Unscented'
options_.ep.stochastic.order = order;
if isequal(method,'Tensor-Gaussian-Quadrature')
    options_.ep.quadrature.nodes = nodes;
elseif isequal(method,'Unscented')
    options_.ep.ut.k = 0;
end

ts = extended_path(oo_.steady_state, 100, innovations(2:end)/sigma, options_, M_, oo_);