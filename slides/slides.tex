\documentclass{beamer}

\usepackage{amsmath,pstricks}
\usepackage{bm}
\usepackage{nicefrac}
\usepackage{mathrsfs}
\usepackage{etex}

\usepackage{pgfplots,tikz}
\usepackage{tikz-qtree}
\usepackage{algorithms/algorithm}
\usepackage{algorithms/algorithmic}
\usepackage[T1]{fontenc}
\usepackage{cancel}
\newenvironment{frcseries}{\fontfamily{frc}\selectfont}{}
\newcommand{\textfrc}[1]{{\frcseries#1}}
\newcommand{\mathfrc}[1]{\text{\textfrc{#1}}}


\newlength\figureheight
\newlength\figurewidth
 
\definecolor{gray}{gray}{0.4}

\begin{document}
\title{The stochastic extended path approach}
\author{St\'ephane Adjemian\footnote{Universit\'e du Maine} and Michel Juillard\footnote{Banque de France}}
\date{June, 2016}

\begin{frame}
  \titlepage{}
\end{frame}

\begin{frame}
  \frametitle{Motivations}

  \begin{itemize}
  \item Severe nonlinearities play sometimes an important role in
    macroeconomics.
  \item In particular occasionally binding constraints: irreversible
    investment, borrowing constraint, ZLB.
  \item Usual local approximation techniques don't work when there are
    kinks.
  \item Deterministic, perfect forward, models can be solved with much
    greater accuracy than stochastic ones.
  \item The extended path approach aims to keep the ability of
    deterministic methods to provide accurate account of nonlinearities. 
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Model to be solved}

\begin{subequations}\label{eq:gmodel}
  \begin{equation}\label{eq:gmodel:1}%\tag{Exogenous states}
    s_t = \mathsf Q (s_{t-1},u_t)
  \end{equation}
  \begin{equation}\label{eq:gmodel:2}%\tag{Euler equations}
    \mathsf F \left(y_t,x_t,s_t,\mathbb E_t \left[ \mathscr E_{t+1}\right]\right) = 0
  \end{equation}
  \begin{equation}\label{eq:gmodel:3}%\tag{Transitions}
    \mathsf G (y_t,x_{t+1},x_t,s_t) = 0
  \end{equation}
  \begin{equation}\label{eq:gmodel:4}%\tag{Auxiliary}
    \mathscr E_{t} = \mathsf H(y_t,x_t,s_t)
  \end{equation}
\end{subequations}

\bigskip

$s_t$ is  a $n_s \times 1$  vector of exogenous state  variables, $u_t
\sim   \mathrm{BB}(0,\Sigma_u)$  is   a  $n_u\times   1$  multivariate
innovation,  $x_t$  is a  $n_x\times  1$  vector of  endogenous  state
variables,  $y_t$  is a  $n_y\times  1$  vector of  non  predetermined
variables and $\mathscr  E_t$ is a $n_{\mathscr E}\times  1$ vector of
auxiliary variables.
\end{frame}


\begin{frame}
  \frametitle{Solving perfect foresight models}

  \begin{itemize}
  \item Perfect foresight models, after a shock economy returns asymptotically
    to equilibirum.
  \item For a long enough simulation, one can consider that for all
    practical purpose the system is back to equilibrium.
  \item This suggests to solve a two value boundary problem with
    initial conditions for some variables (backward looking) and
    terminal conditions for others (forward looking).
  \item In practice, one can use a Newton method to the equations of
    the model stacked over all periods of the simulation.
  \item The Jacobian matrix of the stacked system is very sparse and
    this characteristic must be used to write a practical algorithm.  
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{Extended path approach}

  \begin{itemize}

  \item Already proposed by Fair and Taylor (1983).

  \item The extended path approach creates a stochastic simulation as if only
  the shocks of the current period were random.

  \item Substituting (\ref{eq:gmodel:1}) in (\ref{eq:gmodel:4}), define:
    \[
    \mathscr E_t = \mathscr E (y_t,x_t,s_{t-1},u_t) = \mathsf H(y_t,x_t,\mathsf Q (s_{t-1},u_t))
    \]
  \item The Euler equations (\ref{eq:gmodel:2}) can then be rewritten as:
    \[
    \mathsf F \bigl(y_t,x_t,s_t,\mathbb E_t \left[ \mathscr E (y_{t+1},x_{t+1},s_{t},u_{t+1}) \right]\bigr) = 0
    \]
  \item The Extended path algorithm consists in replacing the previous Euler equations by:
    \[
    \mathsf F \bigl(y_t,x_t,s_t,\mathscr E (y_{t+1},x_{t+1},s_{t},0)\bigr) = 0
    \]
  \end{itemize}

\end{frame}



\begin{frame}
\frametitle{Extended path algorithm}
\algsetup{
linenosize=\small,
linenodelimiter=.
}
\begin{algorithm}[H]
  \caption{Extended path algorithm}
  \label{alg:ep}
  \begin{algorithmic}[1]
    \STATE $H \leftarrow$ Set the horizon of the perfect foresight (PF) model.
    \STATE $(x^\star, y^\star) \leftarrow$ Compute steady state of the model
    \STATE $(s_0, x_1) \leftarrow$ Choose an initial condition for the state variables
    \FOR{$t=1$ to $T$}
    \STATE $u_t  \leftarrow$ Draw random shocks for the current period
    \STATE $(y_t,x_{t+1},s_t) \leftarrow$ Solve a PF with $y_{t+H+1}=y^{\star}$
    \ENDFOR
  \end{algorithmic}
\end{algorithm}
\end{frame}




\begin{frame}
  \frametitle{Extended path algorithm (time $t$ nonlinear problem)}
{\tiny\[
\begin{split}
s_t &= \mathsf Q({\color{red}s_{t-1}},{\color{gray}u_t})\\
0 &= \mathsf F \bigl({\color{blue}y_t},{\color{red}x_t},s_t,\mathscr E (y_{t+1},x_{t+1},s_{t},0)\bigr)\\
0 &= \mathsf G ({\color{blue}y_t},{\color{blue}x_{t+1}},{\color{red}x_t},s_t)\\
s_{t+1} &= \mathsf Q(s_{t},0)\\
0 &= \mathsf F \bigl(y_{t+1},{\color{blue}x_{t+1}},s_{t+1},\mathscr E (y_{t+2},x_{t+2},s_{t+1},0)\bigr)\\
0 &= \mathsf G (y_{t+1},x_{t+2},{\color{blue}x_{t+1}},s_{t+1})\\
&\vdots\\
s_{t+h} &= \mathsf Q(s_{t+h-1},0)\\
0 &= \mathsf F \bigl(y_{t+h},x_{t+h},s_{t+h},\mathscr E (y_{t+h+1},x_{t+h+1},s_{t+h},0)\bigr)\\
0 &= \mathsf G (y_{t+h},x_{t+h+1},x_{t+h},s_{t+h})\\
&\vdots\\
s_{t+H} &= \mathsf Q(s_{t+H-1},0)\\
0 &= \mathsf F \bigl(y_{t+H},x_{t+H},s_{t+H},\mathscr E ({\color{red}y^\star},x_{t+H+1},s_{t+H},0)\bigr)\\
0 &= \mathsf G (y_{t+H},x_{t+H+1},x_{t+H},s_{t+H})\\
\end{split}
\]}
\end{frame}


\begin{frame}
  \frametitle{Extended path algorithm (discussion)}
  \begin{itemize}
  \item This  approach takes full account  of the \emph{deterministic}
    non linearities...
    
    \medskip

  \item ... But  neglects the Jensen inequality by  setting future
    innovations to zero (the expectation).

    \bigskip

  \item We  do not solve  the rational  expectation model! We  solve a
    model  where the  agents  believe  that the  economy  will not  be
    perturbed  in the  future. They  observe new  realizations of  the
    innovations at each date but do not update this belief...
    
    \bigskip

  \item Uncertainty about the future does not matter here.

    \medskip

  \item EP > First order perturbation (certainty equivalence)
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{Stochastic extended path}

  \begin{itemize}
  \item The strong assumption  about future uncertainty can be relaxed
    by    approximating   the    expected   terms    in   the    Euler
    equations~(\ref{eq:gmodel:2})

    \bigskip

  \item We assume that, at time $t$, agents perceive uncertainty about
    realizations   of  $u_{t+1},\dots,u_{t+k}$   but  not   about  the
    realizations of  $u_{t+\tau}$ for all $\tau>k$  (which, again, are
    set to zero)

    \bigskip

  \item Under this assumption, the expectations are approximated using numerical integration.

  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{Gaussian quadrature (univariate)}
  \begin{itemize}
   \item Let $X$ be a Gaussian random variable with mean zero  and  variance  $\sigma^2_x>0$, and suppose that we  need  to  evaluate
     $\mathbb E  [\varphi(X)]$, where  $\varphi$ is a  continuous function.
   \item By definition we have:
     {\small\[
     \mathbb E [\varphi(X)] =  \frac{1}{\sigma_x\sqrt{2\pi}}\int_{-\infty}^{\infty} \varphi(x)e^{-\frac{x^2}{2\sigma^2_x}}\mathrm dx
     \]}
   \item It can be shown that this integral can be approximated by a finite sum using the following result:
     {\small\[
     \int_{-\infty}^{\infty} \varphi(z)e^{-z^2}\mathrm dx = \sum_{i=1}^n\omega_i \varphi(z_i) {\gray + \frac{n!\sqrt{n}}{2^n}\frac{\varphi^{(2n)}(\xi)}{(2n)!}}
     \]} where  $z_i$ ($i=1,\dots,n$)  are the roots  of an  order $n$
   Hermite  polynomial, and  the weights  $\omega_i$ are  positive and
   summing  up to  one (the  error  term is  zero iff  $\varphi$ is  a
   polynomial   of  order   at  most   $2n-1$).  $\rightarrow   x_i  =
   \nicefrac{z_i}{\sigma_x\sqrt{2}}$
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Gaussian quadrature (multivariate)}
  \begin{itemize}
   \item Let $X$ be a multivariate Gaussian random variable with mean zero  and  unit variance, and suppose that we  need  to  evaluate
     {\small\[
     \mathbb E [\varphi(X)] =  (2\pi)^{-\frac{p}{2}}\int_{\mathbb R^{p}} \varphi(\bm{x})e^{-\frac{1}{2} \bm{x}'\bm{x}}\mathrm d\bm{x}
     \]}
   \item Let $\{(\omega_i,z_i)\}_{i=1}^n$ be the weights and nodes of an order $n$ univariate Gaussian quadrature.
   \item This integral can be approximated using a tensor grid:
     {\small\[
     \int_{\mathbb R^{p}} \varphi(\bm{z})e^{-\bm{z}'\bm{z}}\mathrm d\bm{z} \approx \sum_{i_1,\dots,i_p=1}^n\omega_{i_1}\dots\omega_{i_p} \varphi(z_{i_1},\dots,z_{i_p})
     \]}
   \item \textbf{Curse of dimensionality:}  The number of terms in the sum grows exponentially with the number of shocks. 
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Unscented transform}
  \begin{itemize}
   \item Let $X$ be a $p\times 1$ multivariate random variable with mean zero  and  variance $\Sigma_x$. We need to compute moments of $Y = \varphi(X)$.
   \item Let $\mathscr S_p = \{\omega_i,\bm{x}_i\}_{i=1}^{2p+1}$ be a set of deterministic weights and points:
     {\footnotesize
       \[
     \begin{array}{rclcrcl}
       \bm{x}_0 &=& 0  && \omega_0 &=& \frac{\kappa}{p+\kappa} \\
       \bm{x}_i &=& \left(\sqrt{(p+\kappa)\Sigma_x}\right)_i && \omega_i &=& \frac{1}{2(p+\kappa)}\text{, for i=1,\dots,p}\\
       \bm{x}_i &=& -\left(\sqrt{(p+\kappa)\Sigma_x}\right)_{i-p} && \omega_i &=& \frac{1}{2(p+\kappa)}\text{, for i=p+1,\dots,2p}\\
     \end{array}
     \]}
     where $\kappa$ is a real positive scaling parameter.
   \item It can be shown that the weights are positive and summing-up to one and that the first and second order ``sample'' moments of $\mathscr S_p$ are matching those of $X$.
   \item Compute the moments of $Y$ by applying the mapping $\varphi$ to $\mathscr S_p$.
   \item Exact mean and variance of $Y$ for a second order Taylor approximation of $\varphi$. 
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Forward histories (one shock, three nodes, order two SEP)}
  \begin{tikzpicture}
    \tikzset{grow'=right,level distance=80pt}
    \tikzset{execute at begin node=\strut}
    \tikzset{every tree node/.style={anchor=base west}}
    \Tree [.$u_t$ [.$u^1_{t+1}$ [.$u^1_{t+2}$  \edge[white]; \color{red}{$\omega_1\omega_1$} ]
                          [.$u^2_{t+2}$   \edge[white]; \color{red}{$\omega_1\omega_2$} ]
                          [.$u^3_{t+2}$  \edge[white];
                            \color{red}{$\omega_1\omega_3$} ]]
          [.$u^2_{t+1}$ [.$u^1_{t+2}$  \edge[white]; \color{red}{$\omega_2\omega_1$} ]
                          [.$u^2_{t+2}$   \edge[white]; \color{red}{$\omega_2\omega_2$} ]
                          [.$u^3_{t+2}$  \edge[white];
                            \color{red}{$\omega_2\omega_3$} ]]
          [.$u^3_{t+1}$ [.$u^1_{t+2}$  \edge[white]; \color{red}{$\omega_3\omega_1$} ]
                          [.$u^2_{t+2}$   \edge[white]; \color{red}{$\omega_3\omega_2$} ]
                          [.$u^3_{t+2}$  \edge[white]; \color{red}{$\omega_3\omega_3$} ]]
 ]
  \end{tikzpicture}\\
  $\rightarrow$ The tree of histories grows exponentially!
\end{frame}


\begin{frame}
  \frametitle{Fishbone integration}
  \begin{itemize}
    \item The curse of dimensionality can be overcome by pruning the tree of forward histories. 
    \item This can be done by considering that innovations, say, at time $t+1$ and $t+2$ are unrelated variables (even if they share the same name). 
    %\item Analogy with a good  at different periods in an intertemporal general equilibrium model.
    \item If we have $n_u$ innovations and if agents perceive uncertainty for the next $k$ following periods, we consider an integration problem involving $n_u \times k$ unrelated variables.
    \item We use a two points Cubature rule to compute the integral (unscented transform with $\kappa=0$)  $\rightarrow$ The complexity of the integration problem grows linearly with $n_u$ or $k$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Fishbone history (one shock, two nodes, order three SEP)}
  \begin{center}
  \begin{tikzpicture}[grow=right,level distance=2.5cm]
    \node {$u_t$}
    child {
      node {$u_{t+1}=\underline{u}$}
    }
    child {
      node {$\bullet$}
      child {
        node {$u_{t+2}=\underline{u}$}
      }
      child {
        node {$\bullet$}
        child {
          node {$u_{t+3}=\underline{u}$}
        }
        child {
          node {$u_{t+3}=\overline{u}$}
        }
      }
      child {
        node {$u_{t+2}=\overline{u}$}
      }
    }
    child {
      node {$u_{t+1}=\overline{u}$}
    };
  \end{tikzpicture}
  \end{center}
\end{frame}


\begin{frame}
\frametitle{Stochastic extended path algorithm}
\algsetup{
linenosize=\small,
linenodelimiter=.
}
\begin{algorithm}[H]
  \caption{Stochastic Extended path algorithm}
  \label{alg:ep}
  \begin{algorithmic}[1]
    \STATE $H \leftarrow$ Set the horizon of the \emph{stochastic} perfect foresight (SPF) models.
    \STATE $(x^\star, y^\star) \leftarrow$ Compute steady state of the model.
    \STATE $\{(\omega_i,\mathfrc u_i)\}_{i=1}^m \leftarrow$ Get weights and nodes for numerical integration
    \STATE $(s_0, x_1) \leftarrow$ Choose an initial condition for the state variables
    \FOR{$t=1$ to $T$}
    \STATE $u_t  \leftarrow$ Draw random shocks for the current period
    \STATE $(y_t,x_{t+1},s_t) \leftarrow$ Solve a SPF model with $y_{t+H+1}=y^{\star}$
    \ENDFOR
  \end{algorithmic}
\end{algorithm}
\end{frame}

\begin{frame}
  \frametitle{SEP algorithm (order 1, time $t$ nonlinear problem)}
For $i=1,\dots,m$
{\tiny\[
\begin{split}
s_t &= \mathsf Q({\color{red}s_{t-1}},{\color{gray}u_t})\\
0 &= \mathsf F \left({\color{blue}y_t},{\color{red}x_t},s_t,{\sum} _i \omega_i \mathscr E (y_{t+1}^i,x_{t+1},s_{t},\mathfrc u_i)\right)\\
0 &= \mathsf G ({\color{blue}y_t},{\color{blue}x_{t+1}},{\color{red}x_t},s_t)\\
s_{t+1}^i &= \mathsf Q(s_{t},\mathfrc u_i)\\
0 &= \mathsf F \bigl(y_{t+1}^i,{\color{blue}x_{t+1}},s_{t+1}^i,\mathscr E (y_{t+2}^i,x_{t+2}^i,s_{t+1}^i,0)\bigr)\\
0 &= \mathsf G (y_{t+1}^i,x_{t+2}^i,{\color{blue}x_{t+1}},s_{t+1}^i)\\
&\vdots\\
s_{t+h}^i &= \mathsf Q(s_{t+h-1}^i,0)\\
0 &= \mathsf F \bigl(y_{t+h}^i,x_{t+h}^i,s_{t+h}^i,\mathscr E (y_{t+h+1}^i,x_{t+h+1}^i,s_{t+h}^i,0)\bigr)\\
0 &= \mathsf G (y_{t+h}^i,x_{t+h+1}^i,x_{t+h}^i,s_{t+h}^i)\\
&\vdots\\
s_{t+H}^i &= \mathsf Q(s_{t+H-1}^i,0)\\
0 &= \mathsf F \bigl(y_{t+H}^i,x_{t+H}^i,s_{t+H}^i,\mathscr E ({\color{red}y^\star},x_{t+H+1}^i,s_{t+H}^i,0)\bigr)\\
0 &= \mathsf G (y_{t+H}^i,x_{t+H+1}^i,x_{t+H}^i,s_{t+H}^i)\\
\end{split}
\]}
\end{frame}

\begin{frame}
  \frametitle{SEP algorithm (order 2, time $t$ nonlinear problem)}
For all $(i,j)\in \left\{1,\dots,m\right\}^2$
{\tiny\[
\begin{split}
s_t &= \mathsf Q({\color{red}s_{t-1}},{\color{gray}u_t})\\
0 &= \mathsf F \left({\color{blue}y_t},{\color{red}x_t},s_t,{\sum} _i \omega_i \mathscr E (y_{t+1}^i,x_{t+1},s_{t},\mathfrc u_i)\right)\\
0 &= \mathsf G ({\color{blue}y_t},{\color{blue}x_{t+1}},{\color{red}x_t},s_t)\\
s_{t+1}^i &= \mathsf Q(s_{t},\mathfrc u_i)\\
0 &= \mathsf F \bigl(y_{t+1}^i,{\color{blue}x_{t+1}},s_{t+1}^i,{\sum} _j \omega_j \mathscr E (y_{t+2}^{i,j},x_{t+2}^i,s_{t+1}^i,\mathfrc u_j)\bigr)\\
0 &= \mathsf G (y_{t+1}^i,x_{t+2}^i,{\color{blue}x_{t+1}},s_{t+1}^i)\\
s_{t+2}^{i,j} &= \mathsf Q(s_{t+1}^i,\mathfrc u_j)\\
&\vdots\\
s_{t+h}^{i,j} &= \mathsf Q(s_{t+h-1}^{i,j},0)\\
0 &= \mathsf F \bigl(y_{t+h}^{i,j},x_{t+h}^{i,j},s_{t+h}^{i,j},\mathscr E (y_{t+h+1}^{i,j},x_{t+h+1}^{i,j},s_{t+h}^{i,j},0)\bigr)\\
0 &= \mathsf G (y_{t+h}^{i,j},x_{t+h+1}^{i,j},x_{t+h}^{i,j},s_{t+h}^{i,j})\\
&\vdots\\
s_{t+H}^{i,j} &= \mathsf Q(s_{t+H-1}^{i,j},0)\\
0 &= \mathsf F \bigl(y_{t+H}^{i,j},x_{t+H}^{i,j},s_{t+H}^{i,j},\mathscr E ({\color{red}y^\star},x_{t+H+1}^{i,j},s_{t+H}^{i,j},0)\bigr)\\
0 &= \mathsf G (y_{t+H}^{i,j},x_{t+H+1}^{i,j},x_{t+H}^{i,j},s_{t+H}^{i,j})\\
\end{split}
\]}
\end{frame}




\begin{frame}
  \frametitle{Stochastic extended path (discussion)}
  \begin{itemize}
  \item The extended path approach takes full account of the deterministic nonlinearities of the model.
  \item It takes into account the nonlinear effects of future shocks $k$-period ahead.
  \item It neglects the effects of uncertainty in the long run. In most models this effect declines with the discount factor.
  \item The Stochastic Perfect Foresight model, that must be solved at each date, is very large.
  \item Curse of dimensionality with respect with the number of innovations and the order of approximation but \emph{not with the number of state variables}!
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Burnside (1998) model}
\begin{itemize}
\item A representative household
\item A single perishable consumption good produced by a single 'tree'.
\item Household can hold equity to transfer consumption from one period to the next
\item Household's intertemporal utility is given by
\[
\mathbb E_t\left[\sum_{\tau = 0}^\infty
\beta^{ẗ-\tau}\frac{c^\theta_{t+\tau}}{\theta}\right]\quad\quad\mbox{with }
\theta \in (-\infty,0) \cup (0,1]
\]
\item Budget constraint is
\[
p_te_{t+1}+c_t = \left(p_t+d_t\right)e_t
\]
\item Dividends $d_t$ are growing at exogenous rate $x_t$
\[
\begin{split}
d_t &= e^{x_t}d_{t-1}\\
x_t &= (1-\rho)\bar x + \rho x_{t-1}+\epsilon_t
\end{split}
\]
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Dynamics}
  The price/dividend ratio, $y_t = \nicefrac{p_t}{d_t}$, is given by
  \[
  \begin{split}
    y_t &= \beta \mathbb E_t\left[e^{\theta
    x_{t+1}}\left(1+y_{t+1}\right)\right]\\
    x_t &= (1-\rho)\bar x + \rho x_{t-1}+\epsilon_t
  \end{split}
  \]
Iterating forward, $y_t$ can be written as the current value of
future dividends growth rates:
\[
\begin{split}
y_t &=
\mathbb E_t\left[\sum_{i=1}^\infty\beta^\tau e^{\sum_{j=1}^i\theta x_{t+j}}\right]\\
&= \mathbb E_t\left[\sum_{i=1}^\infty\beta^\tau e^{\theta\sum_{j=1}^i\bar
x + \rho^i \hat x_t+\sum_{\ell=1}^j\rho^{j-\ell}\epsilon_{t+\ell}}\right]
\end{split}
\]
with $\hat x_t = x_t-\bar x$.
\end{frame}

\begin{frame}
  \frametitle{The exact solution}
Using formulas for the distribution of the log-normal random variable,
Burnside (1998) shows that the closed form solution is
\[
y_t = \sum_{i=1}^\infty \beta^i e^{a_i+b_i\hat x_t}
\]
where
\[
a_i = \theta \bar x i +
\frac{\theta^2\sigma^2}{2(1-\rho)^2}\left(i-2\rho\frac{1-\rho^i}{1-\rho}+\rho^2\frac{1-\rho^{2i}}{1-\rho^2}\right)
\]
and
\[
b_i = \frac{\theta\rho\left(1-\rho^i\right)}{1-\rho}
\]
\end{frame}

\begin{frame}
  \frametitle{The extended path approach}
In the extended path approach, one sets future shocks to their
expected value, $\mathbb E\left[\epsilon_{t+\ell}\right]=0$, $\ell=1,\ldots,\infty$.
The corresponding solution is given by
\[
\hat y_t = \sum_{i=1}^\infty \beta^i e^{a_i+b_i\hat x_t}
\]
where
\[
a_i = \theta \bar x i {\gray\cancel{+
\frac{\theta^2\sigma^2}{2(1-\rho)^2}\left(i-2\rho\frac{1-\rho^i}{1-\rho}+\rho^2\frac{1-\rho^{2i}}{1-\rho^2}\right)}}
\]
and
\[
b_i = \frac{\theta\rho\left(1-\rho^i\right)}{1-\rho}
\]
\end{frame}

\begin{frame}
  \frametitle{Numerical simulation}
Calibration
\begin{align*}
  \bar x &= 0.0179\\
  \rho &=  -0.139\\
  \theta &= -1.5\\
  \beta &= 0.95\\
  \sigma &= 0.0348\\
\end{align*}
\begin{itemize}
\item The deterministic steady state is equal to 12.3035.
\item The risky steady state, defined as the fix point in absence of
  shock this period:
\[
\widetilde y = \sum_{i=1}^\infty \beta^i e^{\theta \bar x i +
\frac{\theta^2\sigma^2}{2(1-\rho)^2}\left(i-2\rho\frac{1-\rho^i}{1-\rho}+\rho^2\frac{1-\rho^{2i}}{1-\rho^2}\right)}
\]
is equal to 12.4812.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Comparing expended path and closed-form solution} 
Difference between expended path approximation, $\hat y_t$, and closed-form
solution, $y_t$.
\begin{itemize}
\item Using 800 terms to approximate the infinite summation
\item Computing over 30000 periods
\end{itemize}
\begin{align*}
  \min\left(y_t-\hat y_t\right) &= 0.1726\\
  \max\left(y_t-\hat y_t\right) &= 0.1820\\
\end{align*}
\begin{itemize}
\item The effect of future volatility isn't trivial
\[
\frac{\tilde y-\bar y}{\bar y} = 0.0144
\]
\item The effect of future volatility doesn't depend much on the state
  of the economy.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Stochastic extended path}
  \begin{itemize}
  \item A $k$-order stochastic expended path approach computes the conditional
    expectation taking into accounts the shocks over the next $k$ periods.
  \item  The closed formula is 
    \[
    \check{y}_t = \sum_{i=1}^\infty \beta^i e^{a_i+b_i\hat x_t}
    \]
    where
    \[
    a_i = \theta \bar x i +
    \left\{
      \begin{array}{lcc}
        \frac{\theta^2\sigma^2}{2(1-\rho)^2}\left(i-2\rho\frac{1-\rho^i}{1-\rho}+\rho^2\frac{1-\rho^{2i}}{1-\rho^2}\right)
        & \mbox{ for } & i\le k\\
        \frac{\theta^2\sigma^2}{2(1-\rho)^2}\left(k-2\rho\frac{\rho^{i-k}-\rho^i}{1-\rho}+\rho^2\frac{\rho^{2(i-k)}-\rho^{2i}}{1-\rho^2}\right)
        & \mbox{ for } & i > k 
      \end{array}
    \right.
    \]
    and
    \[
    b_i = \frac{\theta\rho\left(1-\rho^i\right)}{1-\rho}
    \]
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Quantitative evaluation}
\begin{itemize}
\item What is the ability of the stochastic extended path approach to
capture the effect of future volatility?
\item What part of the difference between the risky steady state and
  deterministic steady state is captured by different values of $k$?
\item Deterministic steady state: 12.3035
\item Risky steady state: 12.4812
\item The contribution of $k$ future periods
  \begin{center}
    \begin{tabular}{cc}
      $k$ & Percentage\\ 
      1 & 7.4\%\\
      2 & 14.3\%\\
      9 & 50.0\%\\
      30 & 90.1\%\\
      60 & 99.0\%
    \end{tabular}
  \end{center}
\item In such a model, it is extremely costly to give full account of
  the effects of future volatility with the stochastic extended path approach.
\end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Hybrid approach, I}
\begin{itemize}

\item A very large number of  periods forward (the order of stochastic
  extended  path) is  necessary to  obtain an  accurate figure  of the
  effects of future volatility.

\item However, even  a local approximation with a  Taylor expansion of
  low  order provides  better  information on  this  effect of  future
  volatility.

\item This suggests to combine the two approaches.

\end{itemize}
\end{frame}



\begin{frame}
  \frametitle{Hybrid approach, II}
  For $i=1,\dots,\#\{\text{nodes}\}$:
{\tiny\[
\begin{split}
s_t &= \mathsf Q({\color{red}s_{t-1}},{\color{gray}u_t})\\
0 &= \mathsf F \left({\color{blue}y_t},{\color{red}x_t},s_t,{\sum} _i \omega_i \mathscr E \left(y_{t+1}^i+\frac{1}{2}g_{\sigma\sigma},x_{t+1},s_{t},\mathfrc u_i\right)\right)\\
0 &= \mathsf G ({\color{blue}y_t},{\color{blue}x_{t+1}},{\color{red}x_t},s_t)\\
s_{t+1}^i &= \mathsf Q(s_{t},\mathfrc u_i)\\
0 &= \mathsf F \bigl(y_{t+1}^i,{\color{blue}x_{t+1}},s_{t+1}^i,\mathscr E (y_{t+2}^i,x_{t+2}^i,s_{t+1}^i,0)\bigr)\\
0 &= \mathsf G (y_{t+1}^i,x_{t+2}^i,{\color{blue}x_{t+1}},s_{t+1}^i)\\
&\vdots\\
s_{t+h}^i &= \mathsf Q(s_{t+h-1}^i,0)\\
0 &= \mathsf F \bigl(y_{t+h}^i,x_{t+h}^i,s_{t+h}^i,\mathscr E (y_{t+h+1}^i,x_{t+h+1}^i,s_{t+h}^i,0)\bigr)\\
0 &= \mathsf G (y_{t+h}^i,x_{t+h+1}^i,x_{t+h}^i,s_{t+h}^i)\\
&\vdots\\
s_{t+H}^i &= \mathsf Q(s_{t+H-1}^i,0)\\
0 &= \mathsf F \bigl(y_{t+H}^i,x_{t+H}^i,s_{t+H}^i,\mathscr E ({\color{red}y^\star},x_{t+H+1}^i,s_{t+H}^i,0)\bigr)\\
0 &= \mathsf G (y_{t+H}^i,x_{t+H+1}^i,x_{t+H}^i,s_{t+H}^i)\\
\end{split}
\]}
\end{frame}


\begin{frame}
  \frametitle{Hybrid approach, III}

  We compute the difference between the stochastic expended path approximation of order 2, the hybrid approach of order 2 and the closed-form
  solution, $y_t$. We use 800 terms to approximate the infinite summation and run simulations over 30000 periods.

\bigskip
\bigskip

\begin{center}
 \begin{tabular}{c|cc}
   \hline
 & Stochastic & Hybrid stochastic\\
 & extended path & extended path\\ \hline
   maximum difference & 0.1607 & 0.0021\\
   minimum difference & 0.1513 & 0.0019\\ \hline\hline
 \end{tabular}
\end{center}


\end{frame}



\begin{frame}
  \frametitle{Irreversible investment}
Consider the following RBC model with irreversible investment:
\[
  \begin{split}
    \max_{\{c_{t+j},l_{t+j},k_{t+j+1}\}_{j=0}^{\infty}} &\quad \mathcal W_t = \sum_{j=0}^{\infty}\beta^ju(c_{t+j},l_{t+j})\\
           \underline{s.t.}   &  \\
              \qquad y_t &= c_t + i_t\\
              \qquad y_t &= A_tf(k_{t},l_t)\\
              \qquad k_{t+1} &= i_t + (1-\delta)k_{t}\\
              \qquad A_{t} &= {A^{\star}}e^{a_{t}}\\
              \qquad a_{t} &= \rho a_{t-1} + \varepsilon_t\\
              \qquad i_t &\ge 0
  \end{split}
\]
\end{frame}

\begin{frame}
\frametitle{Further specifications}
The utility function is
\[
  u(c_t,l_t) = \frac{\left(c_t^{\theta}(1-l_t)^{1-\theta}\right)^{\tau}}{1-\tau}
\]
and the production function,
\[
  f(k_{t},l_t) = \left(\alpha k_{t}^{\psi} + (1-\alpha)l_t^{\psi}\right)^{\frac{1}{\psi}}
\]
\end{frame}

\begin{frame}
\frametitle{First order conditions}
{\footnotesize\[
\begin{split}
  u_c(c_t,l_t) - \mu_t &= \beta \mathbb E_t\Big[
    u_c(c_{t+1},l_{t+1})\Bigl(A_{t+1}f_k(k_{t+1},l_{t+1}) + 1
    -\delta\Bigr) - \mu_{t+1}(1-\delta)\Big]\\
  \frac{u_{l}(c_t,l_t)}{u_c(c_t,l_t)} &= A_tf_l(k_t,l_t)\\
  c_t + k_{t+1} &= A_tf(k_{t},l_t) + (1-\delta)k_{t}\\
  0 &= \mu_t \left( k_{t+1} - (1-\delta)k_{t} \right)
\end{split}
\]}

\bigskip\bigskip

where $\mu_t$ is the Lagrange multiplier associated with the constraint on investment.
\end{frame}

\begin{frame}
  \frametitle{Calibration}
  \begin{align*}
    \beta    &=  0.990\\
    \theta   &=  0.357\\
    \tau     &=  2.000\\
    \alpha   &=  0.450\\
    \psi     &= -0.500\\
    \delta   &=  0.020\\
    \rho     &=  0.995\\
    A^\star &=  1.000\\
    \sigma   &=  0.100
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Simulation}
  \begin{itemize}
  \item Order: 0, 1, 2 and 3
  \item Integration nodes: 3 (Gaussian quadrature)
  \item Number of periods for auxiliary simulations (SPF): 200
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The trajectory of investment}
\setlength\figureheight{8cm}
\setlength\figurewidth{10cm}
\begin{center}
\input{investment_1.tikz}
\end{center}
\end{frame}

\begin{frame}
  \frametitle{The trajectory of investment}
\setlength\figureheight{8cm}
\setlength\figurewidth{10cm}
\begin{center}
\input{investment_2.tikz}
\end{center}
\end{frame}


\begin{frame}
  \frametitle{Conclusion and future work}
  \begin{itemize}
  \item The extended path approach takes into account effects of nonlinearities.
  \item The stochastic extended path approach takes also partially into account nonlinear effects of future volatility.
  \item Possible to use an hybrid approach, using the risky steady state as terminal condition.
  \item The approach suffers from the curse of dimensionality but it can be mitigated by
    \begin{itemize}
    \item using monomial formulas for integration when there are several shocks
    \item exploiting embarassingly parallel nature of the algorithm
    \end{itemize}
  \end{itemize}
\end{frame}

\end{document}
