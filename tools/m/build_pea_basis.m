function X = build_pea_basis(variables, order)

if order<=0
    X = NaN;
    return
end
    
n = size(variables, 2);

% Compute the number of elements (columns of X)
m = 0;
for k=0:order
    m = m + binomial(n+k-1, k);
end




% Build X
X = zeros(size(variables, 1), m);

powers(3,2)

return
X(:, 1) = 1;
X(:, 1+(1:n)) = variables;
offset = n+1;
for k=2:order
    p = binomial(n+k-1, k);
    for i=1:n
        %for j
    end
    offset = offset+p;
end


function c = binomial(n, k)
    c = factorial(n)/(factorial(k)*factorial(n-k));
    
function p = powers(n, k)
    if isequal(k, 0)
        p = zeros(1, n);
    
    else
        p = [];
        i = 1;
        for l=k:-1:1
            q = powers(n-1, k-l);
            q
            r = [ones(size(q, 1), 1), q];
            p = [p; r];
        end
    end