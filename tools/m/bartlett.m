function w = bartlett(j,m)
    w = 1-j/(m+1);