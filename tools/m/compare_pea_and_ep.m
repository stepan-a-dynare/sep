if ismember(2, pea_orders)
    Z2 = [ones(rows(Z),1), Z, Z(:,1).*Z(:,1), Z(:,1).*Z(:,2), Z(:,2).*Z(:,2)];
    Beta2 =  lsqcurvefit(@pea_predictor, BetaPEA2, Z2, ExpectedTerm, [], [], nlsopt);
    [Beta2, BetaPEA2]
end

if ismember(3, pea_orders)
    Z3 = [Z2, Z(:,1).*Z(:,1).*Z(:,1),  Z(:,1).*Z(:,1).*Z(:,2), Z(:,1).*Z(:,2).*Z(:,2), Z(:,2).*Z(:,2).*Z(:,2)];
    Beta3 =  lsqcurvefit(@pea_predictor, BetaPEA3, Z3, ExpectedTerm, [], [], nlsopt);
    [Beta3, BetaPEA3]
end