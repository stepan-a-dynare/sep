function Gamma = autocovariance(X, order)
    n = size(X,2);
    T = size(X,1);
    Gamma = zeros(n, n, order+1);
    meanX = mean(X);
    X = bsxfun(@minus,X,meanX);
    for i=0:order
        Gamma(:,:,i+1) = (X(1:T-i,:)'*X(1+i:T,:))/T;
    end