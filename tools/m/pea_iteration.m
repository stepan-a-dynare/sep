function params = pea_iteration(params, peamodel, truemodel, samplesize, order, lambda, maxiter, crit, seed, peaopt, nlsopt)
    
    peamodel.M.params(peaopt.offsetparams+1:end) = params;

    set_dynare_seed(seed);
    peamodel.oo = simul_backward_nonlinear_model(truemodel.oo.steady_state, samplesize+10, peamodel.options, peamodel.M, peamodel.oo);

    peamodel.oo.endo_simul(peaopt.idExpectedTerms,:) = 0;
    residuals = evaluate_dynamic_model(peaopt.dynamicmodel, peamodel.oo.endo_simul(:), peamodel.oo.exo_simul, truemodel.M.params, truemodel.oo.steady_state, truemodel.M.lead_lag_incidence, samplesize);
    ExpectedTerm = -residuals(peaopt.idExpectedEquations,peaopt.init:samplesize)';

    X = log(transpose([peamodel.oo.endo_simul(peaopt.idCapital,peaopt.init-1:samplesize-1); peamodel.oo.endo_simul(peaopt.idEfficiency,peaopt.init:samplesize)]));
    switch order
      case 1
        X = [ones(rows(X),1), X];
      case 2
        X = [ones(rows(X),1), X, X(:,1).*X(:,1), X(:,1).*X(:,2), X(:,2).*X(:,2)];
      case 3
        X = [ones(rows(X),1), X, X(:,1).*X(:,1), X(:,1).*X(:,2), X(:,2).*X(:,2), X(:,1).*X(:,1).*X(:,1), X(:,1).*X(:,1).*X(:,2), X(:,1).*X(:,2).*X(:,2), X(:,2).*X(:,2).*X(:,2)];
      otherwise
        error('PEA at order %s is not implemented', order);
    end

    % Run NLS estimation for the expected term.
    [params, resnorm, residual] =  lsqcurvefit(@pea_predictor, params, X, ExpectedTerm, [], [], nlsopt);