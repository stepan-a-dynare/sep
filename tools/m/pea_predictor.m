function predictor = pea_predictor(params, X)
    predictor = exp(X*params);
