function r = pea_objective(params, peamodel, truemodel, samplesize, order, lambda, maxiter, crit, seed, peaopt, nlsopt)
    nparams = pea_iteration(params, peamodel, truemodel, samplesize, order, lambda, maxiter, crit, seed, peaopt, nlsopt);
    r = (params-nparams)'*(params-nparams);