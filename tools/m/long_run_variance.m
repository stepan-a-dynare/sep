function Omega = long_run_variance(X)
    T = size(X,1);
    n = size(X,2);
    m = 4*floor(T^(2/9));
    Gamma = autocovariance(X, m);
    Omega = Gamma(:,:,1);
    for order=1:m
        Omega = Omega + bartlett(order,m)*(Gamma(:,:,order+1)+Gamma(:,:,order+1)');
    end