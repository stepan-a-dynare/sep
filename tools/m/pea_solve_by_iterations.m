function [beta, peamodel] = pea_solve_by_iterations(peamodel, truemodel, samplesize, order, lambda, maxiter, crit, seed, peaopt, nlsopt)

    debug = false;

    % Keep a copy of the PEA parameters
    oldbeta = peamodel.M.params(peaopt.offsetparams+1:end);

    % Set counter.
    iteration = 1;

    info = zeros(maxiter,2);
    s2old = 0;
    while iteration<maxiter
        % Simulate the approximated model.
        set_dynare_seed(seed);
        peamodel.oo = simul_backward_nonlinear_model(truemodel.oo.steady_state, samplesize+10, peamodel.options, peamodel.M, peamodel.oo);

        % Evaluate the residuals
        peamodel.oo.endo_simul(peaopt.idExpectedTerms,:) = 0;
        residuals = evaluate_dynamic_model(peaopt.dynamicmodel, peamodel.oo.endo_simul(:), peamodel.oo.exo_simul, truemodel.M.params, truemodel.oo.steady_state, truemodel.M.lead_lag_incidence, samplesize);
        ExpectedTerm = -residuals(peaopt.idExpectedEquations,peaopt.init:samplesize)';

        % Set the polynomial basis.
        X = log(transpose([peamodel.oo.endo_simul(peaopt.idCapital,peaopt.init-1:samplesize-1); peamodel.oo.endo_simul(peaopt.idEfficiency,peaopt.init:samplesize)]));
        switch order
          case 1
            X = [ones(rows(X),1), X];
          case 2
            X = [ones(rows(X),1), X, X(:,1).*X(:,1), X(:,1).*X(:,2), X(:,2).*X(:,2)];
          case 3
            X = [ones(rows(X),1), X, X(:,1).*X(:,1), X(:,1).*X(:,2), X(:,2).*X(:,2), X(:,1).*X(:,1).*X(:,1), X(:,1).*X(:,1).*X(:,2), X(:,1).*X(:,2).*X(:,2), X(:,2).*X(:,2).*X(:,2)];
          otherwise
            error('PEA at order %s is not implemented', order);
        end

        % Run NLS estimation for the expected term.
        [beta, resnorm, residual] =  lsqcurvefit(@pea_predictor, oldbeta, X, ExpectedTerm, [], [], nlsopt);

        % Update the value of the reduced form parameters.
        newbeta = (1-lambda)*peamodel.M.params(peaopt.offsetparams+1:end)+lambda*beta;

        % Compute absolute value max. difference.
        m = 100*max(abs(newbeta(1:3)-oldbeta(1:3))./abs(oldbeta(1:3)));
        s2 = (residual'*residual)/length(residual);
        ds2 = 100*(s2-s2old)/s2old;
        s2old = s2;
        
        info(iteration,1) = m;
        
        if iteration>1
            info(iteration, 2) = info(iteration, 1)-info(iteration-1, 1);
        end

        peamodel.M.params(peaopt.offsetparams+1:end) = newbeta;
        oldbeta = peamodel.M.params(peaopt.offsetparams+1:end);

        % Print iteration info.
        disp(sprintf('Iteration %s, crit. = %s [lambda=%s][s2=%s][d(s2)=%s]', int2str(iteration), num2str(m, 10), num2str(lambda, 5), num2str(s2, 10), num2str(ds2, 10)));
        
        iteration = iteration + 1;

        % Decide if we reached convergence to the solution.
        if m<crit
            break
        end
    end